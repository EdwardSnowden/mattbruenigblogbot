from typing import Iterator
from logging import info, warning
import time
from dataclasses import dataclass
import requests
import re
from bs4 import BeautifulSoup
from peewee import CharField, Model
import telegram
from telegram import Update
from telegram.ext import Updater, MessageHandler, Filters, CallbackContext, CommandHandler
import html

from .globals import tg_channel_id, db, rhash_matt, updater

MATT_URL = "https://mattbruenig.com/"
MATT_PAGED_URL = "https://mattbruenig.com/page/{}"


# Database
class MattArticleDB(Model):
    id = CharField(primary_key=True)

    class Meta:
        database = db


# Dataclasses
@dataclass
class MattArticle:
    id: str
    header: str
    short: str
    link: str
    date: str
    db_object: MattArticleDB


# Parsers
def parse_matt(page_num=1) -> Iterator[MattArticle]:
    """Parses the mattbruenig blog page and returns all new articles."""
    if page_num < 1:
        warning("Wrong page number.")
        return []
    if page_num == 1:
        page = requests.get(MATT_URL)
    else:
        page = requests.get(MATT_PAGED_URL.format(page_num))
    soup = BeautifulSoup(page.content, 'html.parser')
    articles = soup.find_all("article")
    for article in articles:
        article_id = article['id']
        if MattArticleDB.get_or_none(MattArticleDB.id == article_id):
            continue
        header = article.header.text.strip()
        link_tmp = article.div.a
        if link_tmp is not None:
            link = link_tmp['href']
            link_tmp.decompose()
        else:
            link = article.header.a['href']
        short = article.div.text.strip()
        date = article.footer.span.time['datetime']
        info("Parsing article: %s", header)
        db_obj = MattArticleDB(id=article_id)
        # db_obj.save(force_insert=True)
        yield MattArticle(
            id=article_id, header=header, short=short, link=link, date=date,
            db_object=db_obj
        )


def wait_send(bot, article, **kwargs):
    try:
        message = bot.send_message(**kwargs)
    except telegram.error.RetryAfter as e:
        info(f"Sleeping {e.retry_after} seconds.")
        time.sleep(e.retry_after)
        return wait_send(bot, article, **kwargs)
    article.db_object.save(force_insert=True)
    return message


def send_matt_articles(articles, bot):
    for article in reversed(list(articles)):
        preview_link = f"http://t.me/iv?url={article.link}&rhash={rhash_matt}"
        text = f'<b>{article.header}</b><a href="{preview_link}"> </a>'
        short = html.escape(article.short)
        text += f"\n\n{short}"
        text += f'\n<a href="{article.link}">Continue Reading</a>'
        info(f"Sending {article.header}.")
        wait_send(bot, article, text=text, chat_id=tg_channel_id, parse_mode="HTML")


def update_matt(callback_context: CallbackContext):
    info("Updating matt bruenig blog.")
    info("Parsing.")
    matt_articles = parse_matt()
    info("Sending.")
    send_matt_articles(matt_articles, callback_context.bot)


def send_matt_backlog():
    info("Loading older articles.")
    for page_num in range(155, 0, -1):
        info(f"Loading page {page_num}")
        matt_articles = parse_matt(page_num)
        send_matt_articles(matt_articles, updater.bot)
