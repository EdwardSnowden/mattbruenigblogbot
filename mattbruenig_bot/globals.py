import os
from peewee import SqliteDatabase
from telegram.ext import Updater


# Database Connection
db = SqliteDatabase("volumes/database.db")


# Telegram Bot
updater = Updater(os.environ["BOT_TOKEN"], use_context=True)
dispatcher = updater.dispatcher
tg_channel_id = int(os.environ["CHANNEL_ID"])
rhash_matt = os.environ.get("RHASH_MATT", "ba9f340b40b16f")  # rhash of the instant view
update_time = int(os.environ.get("UPDATE_TIME", 600))
